\documentclass[12pt]{article}

\usepackage[backend=biber,
style=apa,
citestyle=authoryear
]{biblatex}

\addbibresource{bibliography.bib}

\usepackage{graphicx}

\graphicspath{ {./media/} }

\title{NLP and the Philosophy of Language\\Can Transfer Learning lead to a deeper understanding of language?}

\author{Maël Pégny and Juan Luis Gastaldi}

\input{macros.tex}

\begin{document}

\maketitle

In this paper, we explore the hypothesis formulated by Juan Luis Gastaldi that
the success of NLP methods should be taken as a sign that those methods have
learned something about language, and that the extraction and interpretation of
that knowledge from ML methods is a decisive task for linguistics and the
philosophy of language.

This hypothesis can be seen as part of what can only be called the recent «
philosophical moment » of NLP. Researchers on NLP and in formal linguistics have
recently debated, in particular in a very widely followed Twitter thread, on the
exact scope and meaning of NLP success. In particular, the question whether NLP
models can be seen as having access to meaning, one of linguistics’ great
question, has been a topic of heated controversy with many explicit references
to philosophy of language. In broad terms, it can be said that researchers in
NLP and linguistics are wondering what can be inferred from the recent successes
of NLP, whether the current methods can give access to fundamental linguistic
results, and whether they can be seen as a stepping stone towards a more general
form of AI.\GGtodo{I can take care of these two first paragraphs }

In this vast, lively and beautiful debate, we want to take a modest route.
Instead of starting from great philosophical and linguistic questions such as
the nature of meaning and reference or the relevance of the Chinese Room
argument to contemporary ML, we want to start from a particular technical
practice of contemporary NLP, particularly relevant for DNNs, and extract some
philosophical meaning from the current state of the art of that practice. The
practice we want to start from is that of Transfer Learning (TL).
\MPtodo{justification for that approach, which is not the same thing as saying
that it is superior to everybody else’s. It is technically specific, and
specific to something that has been at the core of NLP recent success.}
\GGtodo{Nice way to introduce our approach. I can also take care of this
paragraph}\GGtodo{This is probably the moment to present the whole paper to come
as end of the intro, and add section title “Transfer Learning” before starting
the next paragraph}

\GGtodo{I’d start with a definition of what we understand by TL (which might not
be trivial btw)}There exists two implicitly distinct views of transfer learning.
The first is found more commonly in image processing, and conceives transfer
learning as a local transfer from one task and/or domain to another. The other,
more common in NLP, is to conceive TL as the fine-tuning to a given task
\GGtodo{Examples and references?}\GGtodo{Not sure this idea of “fine-tuning” is
correct to speak of TL here. The link between both doesn’t seem straightforward
to me} or domain of a ``general model of language", or simply « model of
language ». This second approach comes from the success of TL starting from
parts of a model trained to predict speech parts (usually words, but sometimes
parts of sentences or full sentences) from their context. This success has been
theorized this success by the hypothesis \MPtodo{how common ?} that models
trained on this specific task develop a robust representation of language which
is then useful for other tasks. In other words, this particular class of
predictive DL models learn something generic about language, and it is that
general knowledge that is at the root of the success of transfer learning. After
all, that’s the justification for calling a model trained on a specific task,
that of predicting parts of speech from their context, a general model of
language.\GGtodo{I find this passage rather confusing, and I’m not sure what
point you want to make. I can work on it, but we should discuss what is the
specific argument} The examination of transfer learning provides a particular
form of our general argument that the success of NLP might be seen as the sign
of a robust knowledge on language.

This particular argument is often tied to a discussion of the distributional
semantics,\GGtodo{Well, actually it’s a bit more complicated. In theory, it
should be as you said, but in practice it’s not necessarily the case. My feeling
is that the distributional argument was rather used bu the CS community, while
the linguistic community, which raises the question of the generality of models,
does not seem to take the distributional argument into serious account. We can
discuss about it to see how we present things.} that is the hypothesis that the
meaning of a word or linguistic unit is captured by its relative frequency of
occurrences in a set of contexts.\GGtodo{I can make this a little more precise}
This particular view of semantics, with roots in linguistics far pre-dating the
recent rise of NLP, is itself often tied to the general philosophy of language
that « meaning is use », with a particular statistical twist on the concept of
use. If this is of course a very interesting issue, we want to insist on the
fact that the philosophical significance of transfer learning based on general
models of language can be decoupled, if only as a methodological step, from many
particular beliefs on semantics.\GGtodo{Ok, nice point} Transfer learning based
on general models of language has been a core component of the recent successes
of NLP, because it helps building performant models for many tasks,\GGtodo{Not
sure this is accurate. I’d say that TL has played a rather marginal role in
research (although an important role in democratizing the models).} even though
we will see that the « performant relative to what ? » question is a thorny
issue. This ability to find a general use to a particular predictive model
warrants in itself our hypothesis that transfer learning may be the sign that
NLP has reach some form of robust knowledge of language, no matter what
particular linguistic hypothesis is made on the justification of this
success.\GGtodo{Ok, so I realize at this point that our conceptions of TL do not
necessarily coincide. You seem to be calling TL what to me is rather
“pre-trained models”, while I was restricting TL to techniques like
distillation. Not sure whether your conception is too broad or mine too narrow
(or both). In any case, we should adjust our definitions and have a solid
justification of it.} We will call this approach the « minimalistic approach. »
\MPtodo{this could easily be put later in the paper.}

This argument is also bound to play a role in the debate over the
interpretability of opaque models in ML. Deep Learning NLP models, especially
extremely large ones, are a paradigm of opaque models. Opaque model can be
presented as the fruit of forcing generic statistical methods on phenomena
without  presupposing or producing any structural understanding of those
phenomena. Those models would then produce brute predictive performances, but
would not produce anything like structural knowledge\GGtodo{What do you mean by
that ?} of their target phenomenon. From this standpoint, explainability\GGtodo{Are you shifting from interpretability (beginning of the paragraph) to explainability or are these two different things?} methods
would help debug programs and improve the understanding of those generic
methods, but they could not be exploited to generate new fundamental scientific
knowledge on target phenomena.

There are many ways in which this view of the problem of opacity can be
challenged. However, it seems that transfer learning offers a particularly
fruitful angle on this debate. It allows to nuance the conception of
contemporary, ML-driven AI as pure weak AI, solely focus on specialized systems.
Between general scientific theories and pure specialized systems, there is a
third option, which is the slow and tentative generalization of the knowledge
built for a particular task  and/or domain to other tasks and domains. This
creates a path towards robust, general knowledge that is specific to
contemporary AI, and in particular to opaque AI, as transfer learning is widely
used for DNNs.\GGtodo{What do you have in mind ?} If ML models can produce more
than just predictive performance over a specific task, it becomes harder to
resist the temptation to think those models generalize because they produce
generalizable knowledge on their target, albeit in a form that is somewhat
obscure to us. This paper is thus dedicated to the following question: does
transfer learning demonstrate that DL NLP models, especially so-called general
models, acquire robust\GGtodo{The notion of robust appeared already a couple of
times, and deserves some treatment. As far as I understand, in the field the
idea of robust is tied to the (in)sensitivity to small variations in the input
(the less sensitive, the more robust). But if we assume this definition, it is
not clear how or why general models in the sense of TL (or “pre-trained” models)
is connected to robustness.} knowledge on language?

In particular, the fact that subparts of a DNN, such as one or several hidden
layers trained for a given task may be re-used to boost the performances of a
different tasks challenge the image of DNNs s pure black boxes, solely
fine-tuned for the optimization of a given task. If subparts of the model can be
used outside of their original narrow task, it may indicate that those hidden
layers contain robust knowledge that can be abstracted away from its initial
context of formation. We will call this the « modularity argument,» as transfer
learning of subparts of a model may be seen as a elementary form of modularity
for opaque ML models, challenging the assumption that those models are just «
big balls of wax » dedicated to particular tasks.

\section{Technical presentation}

To understand transfer learning for supervised learning, a couple of definitions
are in order.\MPtodo{what happens for other forms of learning ?}

The pair D = (X, P(X)), where X is our data sample, and P(X) is a probability
distribution over that sample X, which is assumed to exist, is called the
learning domain. The couple (Y, (P(Y｜X)) where Y is our target variable, and
P(Y ｜ X) is the probability distribution that we are trying to approximate
through p(y ｜ x), is our learning task. For transfer learning to happen, we
must have a source learning domain Ds and a source learning task Ts, and a
target Dt and a target learning task Tt. \MPtodo{is there a name for the pair
(D,T) ?} For transfer learning to be of any interest, the pairs (Ds, Ts) )
(Dt,Tt) must be different, that is to say at least one of their elements must be
different from the corresponding element in the other pair : Dt  ≠ Ds or Ts  ≠
Tt. In practice, both task and domain can be different.

Since D and T are also pairs, they can be different if at least one of the
corresponding elements are different : Ds  ≠ Dt if  Xs  ≠ Xt or P(Xs)  ≠ P(Xt)
and Ts ≠ Tt iff Ys  ≠ Yt or P(Ys ｜ Xs)  ≠ P(Yt ｜ Xt).  Let us examine all
those cases:

Ds  ≠ Dt

Xs  ≠ Xt. For instance, Xs might be made of color images, while Xt might be made
of grayscale image. or both datasets may be texts in different languages.

P(Xs)  ≠ P(Xt). For instance, if the source domain is made of hand drawn images
and the target domain of photographs, abstract figures might be much more common
in hand drawn images than in photographs. Still in image recognition, the same
phenomenon might happen if we take pictures of the same places or objects in
different conditions. Or we may consider two sets of documents written in the
same language, but with different topics, radically affecting the probability
distributions of some terms. This is very common, and is called « domain
adaptation ».

Ts  ≠ Tt

Ys  ≠ Yt. If we consider a classification task, we may have more classes in Yt
than in Ys.

P(Ys ｜ Xs)   ≠ P(Yt ｜ Xt). For instance, class imbalance might be completely
different.

In practice, extra constraints are often added, such as little or no labels
available, but we will ignore this for now.

\MPtodo{the Technion presentation suppose input similarity between the two
models : the target model has to be able to take the same kind of input as the
source model. This is not true in the NLP case presented by Thomas Wolf }

From a heuristic point of view, transfer learning is expected to take place
between intuitively similar domains and/or tasks. The similarity may be very
strong, as DNNs may have difficulties to perform generalization seemingly
obvious to humans. For instance, a DNN trained to recognize written digits on
color image may have difficult to deal with grayscale image. The complete
irrelevance of color for digit recognition, even if it is obvious for humans,
might still be hard for a model with decent performances \MPtodo{check
performances level} on color images.  This creates a heuristic difficulty for
TL, as transfer to a seemingly analogous task and/or domain might end up in a
clear failure. A technique might then be to create domain-confusion, that is to
abolish the difference between the two domains because we know it is irrelevant.
That is a different approach to generalization happening at the data cleaning
and structuring level. Be it as it may, the generalization offered by TL
operates locally from a given (Ds, Ts) pair to another (Dt, Tt). 

As we have already mentioned in the introduction, the intuitive vision coming
from the practice of TL in NLP is quite different. We have there a privileged
task, or set of tasks in more formal terms, that acts a source task for many
other tasks and/or domains. This set of tasks is part-of-speech
prediction.\GGtodo{Not really. POS tagging is a different task. It’s not the
same to predict that the next word is “dog”, than to predict that the next word
is a noun. The latter would be “POS prediction”, the former is language
modelling or “cloze task”} \MPtodo{how do we formalize this, as I do not think
that the notion of part of speech is well formalized?\reply{I’m not convinced
that the model of TL you set up in the previous paragraphs can describe what
pre-trained models do (what you call TL in NLP). Maybe this is the case, or you
have something specific in mind, you’ll have to explain it to me.}}

\section{TL and the quest for optimal performances}

\subsection{The Skeptic Argument Against TL from A Fundamental Theorem of ML	}

A major objection might be made to the hypothesis that TL is a path towards more
robust knowledge of language. In ML, transfer learning is considered a
second-best option compared to dedicated training from scratch with a large,
high quality database. More importantly, this view is grounded in a theoretical
argument based on a fundamental theorem of Machine Learning. This theorem states
that model error on a finite database, or empirical risk, tends towards the
minimal error achievable for a task, or Bayesian risk, when database size goes
to infinity. From this theoretical vantage point, nothing can be possibly better
for a task than to train a model from scratch with the largest possible
database. TL might be a very practical solution to the practical challenges
raised by the high computational costs of training, or the absence or high costs
of large high quality databases for every single task. However, from a
theoretical vantage point, TL is not the ideal path to performance, and as such
it should not be considered a path towards the best knowledge ML can produce. We
will call this argument « the skeptic theoretical argument against TL as a path
to more robust knowledge of language », or « skeptic theoretical argument », for
short.

However, the practical implications of this theoretical argument, as many limit
arguments in empirical science, needs to be debated with the greatest care. In
practice, two things cannot be taken for granted :

a)	The availability of a dedicated database for every conceivable task under the
Sun.

a)	The possibility to get those databases to grow in size undefinitely.

In simple terms, the limit argument given by the fundamental theorem of ML does
not provide a realistic path towards best possible performance for every
possible task, because the expectation that we might be able to have
ever-growing databases for every single task is not realistic. In face of those
practical but nevertheless hard constraints, how are we to achieve the best
possible performance? An obvious answer to this might be TL, as it allows to
leverage the knowledge acquired on tasks for which large scale databases are
available for tasks where conditions (a) or (b) are not met. If TL might be a
second best option compared to an ideal theoretical target which we will never
reach, it might be the best path towards performance if realistic constraints
are taken into account.\GGtodo{Nice point}

\MPtodo{Could we make a similar argument on practical constraints with
computational cost ? I would need to know how computational cost grows with
datasize in DL in order to see if hard computational constraints could hit us
before datasize limits hits us.}

 ii) Discussion of performance metrics. According to Christophe, the fundamental
 theorem can be adapted to various metrics by changing the definition of
 risk,\GGtodo{By « risk » do you mean “loss”?} so the argument remains relevant
 no matter what mathematical metrics we use. However, if some linguistic have no
 well-defined performance metrics, then this theoretical argument does not
 apply. However, it does mean that TL would fare better in those cases (how
 would we know that in the absence of clearly defined performance metrics
 \GGtodo{The problem here is that with no clear metric, nothing is possible
 actually (I bumped into this shameful situation with my own model :/ ). In
 other words, if there are no clear metrics, why do ML at all?}?). We also have
 to wonder « performances for what ? » as there is a debate in NLP between
 purely linguistic targets and more « general AI » target.  Ultimately, what is
 the relation between robust knowledge of language and performance, especially
 predictive performances? 

iii) Is general knowledge produced by ML going to be something that linguists
would consider linguistic knowledge? ML might be able to find general patterns
in textual data that are interesting but may not be what linguists expect of
general knowledge of language. Very interesting argument from Christophe saying
that he sees some results as results on information, not really on language per
se. \GGtodo{There have been a lot of work on this in the past 2/3 years. Cf. for
instance the “domain” (or rather “practice”) of “Bertology”. More generally,
there is a whole work on “probing” stemming from this problem. I could write
about it, but first we need to decide what will be the place of it in our
argument}

In the particular context of NLP, this argument against TL as a path towards
robust knowledge can take a particular form due to the singularity of general
model of languages, and their privileged role in TL for NLP. The task of
predicting  units of language as the specificity of being self-supervised : the
unit of language can be used as its own label. Consequently, not only is it easy
to gather gigantic digital corpora today, but a general model of language can be
trained without the slow, manual and highly costly work of corpus annotation.
This made the training of general models of language on titanic corpora possible
at a relatively low cost. It may thus be possible to argue that the privileged
role of general model of language in NLP is due to just that easy access to huge
databases enabled by the self-supervised nature of the task. General models of
language might not owe their strategic role to a particularly robust linguistic
knowledge, but to the fact that they have been trained on huge databases. 

However, this skeptic argument \GGtodo{Is someone actually holding such an
argument explicitly? I can’t imagine anyone seriously backing it up, but maybe
you’ve seen it around. I would be curious to see a more elaborated version of
it.} does nothing to explain why the transfer can be a success for tasks that
are intuitively very different from prediction of language units. If database
size is something we should always keep in mind, the particular form of the
skeptic theoretical argument ultimately begs the question.  \MPtodo{more can be
said on this for sure : it goes to show that « size » and « robust knowledge »
are not to be squared in opposition to one another : one of the reasons why
predictive language models are the source of robust knowledge may not be only
about the particular task or the way it is dealt with matrices of co-occurrences
and the distributionalist semantics, but because it has been able to detect
important statistical patterns in huge corpora that are representative of a good
chunk of linguistic practice. \reply{Not sure what you mean by this. The notion
of “pattern” is too vague to be able to mean something, and it’s not clear
either what it means to be “representative of linguistic practice”}}

Ultimately, the skeptic theoretical argument starts a conversation on the
relation between robust knowledge of language and performance, especially
predictive performance. In philosophy of science,  general knowledge is supposed
to unify pre-existing knowledge in an economic fashion, and to increase
predictive performances : that’s what we would expect in physics for instance.
However, the case of ML is very singular in that respect, as the fundamental
theorem we mentioned tells us that in terms of predictive performances, nothing
can beat training with an arbitrarily large dedicated database. In a rough,
intuitive sense, this fundamental theorem states that general knowledge cannot
beat dedicated training for a specific task, which goes against many
well-entranched beliefs on the power of generalization coming especially from
the history of physics. In a strange turn of methodological events, ML might go
in the exact opposite direction that the conventional wisdom inspired by physics
may seem to indicate.\GGtodo{I think this a great point that needs to be further
developed and hava a more central role in the paper (maybe even from the very
beginning, to frame the problem). The relation between prediction and
generalization is at the center of this whole story, and the way in which both
are tied in language can contribute enormously to make things clearer, taking
distance both from an epistemology modeled upon physics and another one
following Data Science.} In physics, the construction of ad hoc models of a
particular phenomenon might seem as a second best option in the absence of solid
theoretical understanding of the phenomenon. In ML, our fundamental theorem
tells us that  general knowledge is a second-best possibility in the absence of
dedicated ad hoc models. If that is the case, that means that we cannot infer
that we have reached robust knowledge from the fact that our predictive
performances have significantly increased, as we would in other domains of
predictive modelization, because predictive performances would only be increased
relative to a weak dedicated training on a too small database. Our fundamental
theorem of ML gives thus a very particular context to the usual conversation on
generalization and predictive performances. Even though we have seen that
practical constraints still allow to defend TL in the name of performances in a
realistic setting, the theoretical foundations of statistical learning calls
into question the common assumption that we should seek general knowledge
because it is the golden path to predictive performances in the long
run.\GGtodo{I have another argument against the relevance of the fundamental
theorem to provide an adequate epistemology for NLP or language theory.
Languages are grammars. Grammars are about compression. More data, memory and
computational power tend to a memorization of the data (no compression at all).
Overfitting problem. The statistical approach to language, framed in the sense
of the fundamental theorem of ML, is not the right framework to think about
language. The question should be something like: what is the point where the
compression rate is maximal. In this sense, recent work on formal linguistic
(Yang, "The price of linguistic productivity") suggests (and shows to a great
extent) that the smaller the corpus, the bigger the chances to generalize (the
argument is technical, backed up with a lot of empirical data). From this, we
could draw the cool conclusion that the epistemology of statistics is not the
right one for NLP (and maybe ML more generally?), not less than that of physics.
Another epistemology is necessary, and we have contributed to go in that
direction.}

\newpage

\section{Notes}

\subsection{References}

« Tutorial 6. TL \& Domain Adaptation ». Technion, Israël for the formalization
of TL

Thomas Wolf, TL for NLP for the presentation of TL in NLP through general
purpose models.

\subsection{References to be read}

Causal transfer learning?

The keras Blog, Hugging Face. See « How Convolutional Networks View the World »

See Paul \& Yang, 2010

\subsection{First reactions by Gianni}

Need to re-examine the legitimacy of specialized performances. It may be natural
to be more performant through specialization, but is it always the goal ?

Necessary to take into account the size of the model : transferred model are
just sometimes smaller than larger models. What of a comparison at constant
size?

Class information coming through transfer learning : the word belongs to a
certain class. That’s not an explanation, but that’s an information to be used.
Remember explainability not as a bonus, but as a part of the task.

\subsection{Possible plan for the article}

\begin{itemize}
    \item Epistemological import of AI
    \item Focus on a specific case: NLP
    \item Focus on a specific phenomenon: Transfer Learning (TL)
    \item Stage the problem: AI models are performant. Performance does not entail knowledge. Ex: Ways to get to a result might be foreign to any (natural or even understandable) way of relating to the knowledge of a phenomenon (in this case, language). Ex: “Paul tells a story”; “Paul tells John” [shitty example, I’ll find a more adequate one]. Maybe the model arrives at good results without constructing two categories (direct and indirect object) but one in which both “a story” and “Paul” live.
    \begin{itemize}
        \item Of course, that completely different way to relate to language could constitute a knowledge, but since units are not explicit, we cannot even know
    \end{itemize}
    \item Knowledge is about generality. (Chollet: we should train directly for generality). This contrasts with the task specific orientation
    \begin{itemize}
        \item Difference between generalization as performance over unseen data, and generalization as performance on a different task. The field is usually focused on the first one, but the second one is more interesting
    \end{itemize}
    \item Some models are indeed general (in the second sense)
    \item More significantly, TL only works by addressing such generality
    \begin{itemize}
        \item This is why focusing in TL is a good way to identify what is general in AI models (and NLP more specifically)
    \end{itemize}
    \item Two cases of TL as case studies
    \begin{itemize}
        \item Word Embeddings
        \item Distillation
    \end{itemize}
    \item What can we learn from them?
    \begin{itemize}
        \item Both are cases relating to the specific task of “language modelling” (close task).
        \begin{itemize}
            \item There is something general about it
        \end{itemize}
        \item Reference to the “Semantics mega thread”
        \item Instead of Chinese room or general “philosophy of language”, raise the question of Distributionalism
        \item The two cases show distributionalism working at two levels: linguistic units (WE) and classes of those units (Distillation)
        \item This is what is general in language. The success of TL shows (somewhat) that the models have it
    \end{itemize}
    \item Argument against critiques of TL as a “pis aller”
    \item Conclusions
\end{itemize}


\printbibliography

\end{document}